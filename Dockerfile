FROM node:16 AS build

WORKDIR /src
COPY . .

RUN npm install && npm run build


FROM nginx:alpine

COPY --from=build /src/build /usr/share/nginx/html
COPY --from=build /src/nginx.conf /etc/nginx/conf.d/default.conf

ENTRYPOINT ["nginx", "-g", "daemon off;"]