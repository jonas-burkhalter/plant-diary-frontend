import React, {useState} from "react";
import {BrowserRouter, Routes, Route} from "react-router-dom";

import {AppBar, Toolbar, Typography} from "@mui/material";
import {Cancel} from '@mui/icons-material';
import {closeSnackbar, SnackbarProvider} from 'notistack';

import PlantCreateView from "./views/plants/views/Create";
import PlantDetailView from "./views/plants/views/Detail";
import PlantListView from "./views/plants/views/List";
import PlantEditView from "./views/plants/views/Edit";

const App: React.FC = () => {
    const [navigation, setNavigation] = useState(false);

    return (
        <SnackbarProvider
            action={(snackbarId) => (
                <Cancel onClick={() => closeSnackbar(snackbarId)}/>
            )}
            anchorOrigin={{horizontal: "left", vertical: "top"}}
            maxSnack={5}
        >
            {/*<Drawer open={navigation} onClose={() => setNavigation(false)}>*/}
            {/*    <Navigation />*/}
            {/*</Drawer>*/}

            <AppBar className="AppBar" position="static">
                <Toolbar>
                    {/*<IconButton*/}
                    {/*    size="large"*/}
                    {/*    edge="start"*/}
                    {/*    color="inherit"*/}
                    {/*    aria-label="menu"*/}
                    {/*    sx={{ mr: 2 }}*/}

                    {/*    onClick={() => setNavigation(true)}*/}
                    {/*>*/}
                    {/*    <MenuIcon />*/}
                    {/*</IconButton>*/}
                    <Typography variant="h6" component="div" sx={{flexGrow: 1}}>
                        Plant Diary
                    </Typography>
                </Toolbar>
            </AppBar>

            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<PlantListView/>}/>
                    <Route path="plants" element={<PlantListView/>}/>
                    <Route path="plants/create" element={<PlantCreateView/>}/>
                    <Route path="plants/:plantId" element={<PlantDetailView/>}/>
                    <Route path="plants/:plantId/edit" element={<PlantEditView/>}/>
                </Routes>
            </BrowserRouter>
        </SnackbarProvider>
    );
};

export default App;
