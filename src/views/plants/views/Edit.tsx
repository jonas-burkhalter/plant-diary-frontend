import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";

import {Card, CardHeader, Container} from "@mui/material";

import Plant, {DEFAULT as DEFAULT_PLANT} from "../models/Plant";
import PlantPicture, {DEFAULT as DEFAULT_PLANT_PICTURE} from "../models/PlantPicture";
import {plantService} from "../services/PlantService";
import {plantPictureService} from "../services/PlantPictureService";
import CreateOrEdit from "../components/CreateOrEdit";

const PlantEditView: React.FC = () => {
    const navigation = useNavigate();
    const {plantId} = useParams();

    const [plant, setPlant] = useState<Plant | null>(null);
    const [plantPictures, setPlantPictures] = useState<Array<PlantPicture> | null>(null);

    useEffect(() => {
        plantService.get(Number(plantId)).then((result) => {
            setPlant(result);
        });
    }, [plantId]);

    useEffect(() => {
        plantPictureService.getAll(Number(plantId)).then((result) => {
            setPlantPictures(result);
        });
    }, [plantId]);

    function edit(plant: Plant, plantPictures: Array<PlantPicture>) {
        plantService.edit(plant).then(plantId => {
            // plantPictureService.create(plantId, plantPicture).then(r =>
            //     navigation("/plants")
            // );
        })
    }

    return (
        <Container maxWidth="md">
            {plant && plantPictures && (
                <Card>
                    <CardHeader title="Edit Plant"/>

                    <CreateOrEdit plant={plant} plantPictures={plantPictures} onSubmit={edit}/>
                </Card>
            )}
        </Container>
    );
};

export default PlantEditView;
