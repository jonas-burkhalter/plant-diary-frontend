import React, {ChangeEvent, useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {useFormik} from "formik";
import * as Yup from 'yup';

import Plant, {DEFAULT as DEFAULT_PLANT} from "../models/Plant";
import PlantPicture, {DEFAULT as DEFAULT_PLANT_PICTURE} from "../models/PlantPicture";
import {plantService} from "../services/PlantService";
import {plantPictureService} from "../services/PlantPictureService";
import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    Container, Input, Grid, TextField
} from "@mui/material";

const PlantDetailView: React.FC = () => {
    const navigation = useNavigate();
    const {plantId} = useParams();

    const [plant, setPlant] = useState<Plant>(DEFAULT_PLANT);
    const [plantPictures, setPlantPictures] = useState<Array<PlantPicture>>([]);

    useEffect(() => {
        plantService.get(Number(plantId)).then((result) => {
            setPlant(result);
        });
    }, [plantId]);

    useEffect(() => {
        plantPictureService.getAll(Number(plantId)).then((result) => {
            setPlantPictures(result);
        });
    }, [plantId]);

    return (
        <Container maxWidth="lg">

            <Card>
                <CardHeader title={plant.name}/>

                <CardContent>
                    <Grid container spacing={2}>
                        {plantPictures?.map((plantPicture) => (
                            <Grid item xs={8}>
                                <img src={`data:image/png;base64,${plantPicture.data}`}/>

                            </Grid>
                        ))}
                    </Grid>

                </CardContent>
            </Card>
        </Container>

    );
};

export default PlantDetailView;
