import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";

import Plant from "../models/Plant";
import {plantService} from "../services/PlantService";
import {Card, CardActionArea, CardContent, CardHeader, Grid} from "@mui/material";

const PlantListView: React.FC = () => {
    const navigation = useNavigate();
    const [plants, setPlants] = useState<Array<Plant>>([]);

    useEffect(() => {
        plantService.getAll().then((result) => {
            setPlants(result);
        });
    }, []);

    function goToDetail(plant: Plant) {
        navigation(`/plants/${plant.id}`);
    }

    return (
        <Grid container spacing={2}>
            {plants?.map((plant) => (
                <Grid item md={6} xs={12}>
                    <Card>
                        <CardActionArea onClick={() => goToDetail(plant)}>
                            <CardHeader title={plant.name}/>

                            <CardContent>
                                <Grid container spacing={2}>
                                    <Grid item xs={8}>
                                        {plant.name}
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                </Grid>
            ))}
        </Grid>

        /*
        <IonContent fullscreen>
            <IonFab slot="fixed" vertical="top" horizontal="end">
                <IonFabButton>
                    <IonIcon icon={add} onClick={() => history.push("/plants/create")}/>
                </IonFabButton>
            </IonFab>
            <IonRow>
                {plants?.map((plant) => (
                    <IonCol size="12" sizeMd="6" key={plant.name}>
                        <IonCard button href={(plant?.name)}>
                            <IonCardHeader>
                                <IonCardTitle>{plant?.name}</IonCardTitle>
                                <IonCardSubtitle>{plant?.name}</IonCardSubtitle>
                            </IonCardHeader>

                            <IonCardContent>
                                <IonButton routerLink={(plant?.name)}>
                                    Weiter
                                </IonButton>
                            </IonCardContent>
                        </IonCard>
                    </IonCol>
                ))}
            </IonRow></Grid>
        </IonContent>
         */
    );
};

export default PlantListView;
