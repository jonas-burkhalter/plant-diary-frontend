import React, {useState} from "react";
import {useNavigate} from "react-router-dom";

import {Card, CardHeader, Container} from "@mui/material";

import Plant, {DEFAULT as DEFAULT_PLANT} from "../models/Plant";
import PlantPicture, {DEFAULT as DEFAULT_PLANT_PICTURE} from "../models/PlantPicture";
import {plantService} from "../services/PlantService";
import {plantPictureService} from "../services/PlantPictureService";
import CreateOrEdit from "../components/CreateOrEdit";

const PlantCreateView: React.FC = () => {
    const navigation = useNavigate();
    const [plantPicture, setPlantPicture] = useState<PlantPicture>(DEFAULT_PLANT_PICTURE);

    function create(plant: Plant, plantPictures: Array<PlantPicture>) {
        plantService.create(plant).then(plantId => {
            createPlantPictures(plantId, plantPictures);
        });
    }

    function createPlantPictures(plantId: number, plantPictures: Array<PlantPicture>) {
        const promises: Array<Promise<any>> = [];

        for (const plantPicture of plantPictures) {
            promises.push(plantPictureService.create(plantId, plantPicture));
        }

        Promise.all(promises).then(r => {
            navigation("/plants")
        });
    }

    return (
        <Container maxWidth="md">
            <Card>
                <CardHeader title="Create Plant"/>

                <CreateOrEdit plant={DEFAULT_PLANT} plantPictures={[]} onSubmit={create}/>
            </Card>
        </Container>
    );
};

export default PlantCreateView;
