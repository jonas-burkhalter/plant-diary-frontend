import BaseServie from "../../../services/BaseService";
import Plant from "../models/Plant";

class PlantService extends BaseServie {
    private readonly RESSOURCE_URL: string;

    constructor(base_url: string = "") {
        super();
        console.debug(`${PlantService.name}::constructor`, base_url);

        this.RESSOURCE_URL = `${this.BASE_URL}/plant`;
    }

    public async create(plant: Plant): Promise<number> {
        return super.POST(`${this.RESSOURCE_URL}`, plant);
    }

    public async edit(plant: Plant): Promise<number> {
        return super.PUT(`${this.RESSOURCE_URL}/${plant.id}`, plant);
    }

    public async getAll(): Promise<Array<Plant>> {
        return super.GET(`${this.RESSOURCE_URL}`);
    }

    public async get(id: number): Promise<Plant> {
        return super.GET(`${this.RESSOURCE_URL}/${id}`);
    }
}

export const plantService = new PlantService();
