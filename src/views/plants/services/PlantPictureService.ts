import BaseServie from "../../../services/BaseService";
import PlantPicture from "../models/PlantPicture";

class PlantPictureService extends BaseServie {
    private readonly RESSOURCE_URL: string;

    constructor(base_url: string = "") {
        super();
        console.debug(`${PlantPictureService.name}::constructor`, base_url);

        this.RESSOURCE_URL = `${this.BASE_URL}/plant`;
    }

    public async create(plantId: number, plantPicture: PlantPicture): Promise<number> {
        return super.POST(`${this.RESSOURCE_URL}/${plantId}/picture`, plantPicture);
    }

    public async getAll(plantId: number): Promise<Array<PlantPicture>> {
        return super.GET(`${this.RESSOURCE_URL}/${plantId}/picture`);
    }
}

export const plantPictureService = new PlantPictureService();
