import React, {useEffect, useState} from "react";

import {useFormik} from "formik";
import * as Yup from 'yup';
import {Button, CardActions, CardContent, Grid, TextField} from "@mui/material";

import MultipleFileUpload from "../../../components/files/FileUploadMultiple";
import File from "../../../components/files/File";

import Plant from "../models/Plant";
import PlantPicture from "../models/PlantPicture";

interface PlantCreateOrEditComponentProps {
    plant: Plant,
    plantPictures: Array<PlantPicture>,

    onSubmit: (plant: Plant, plantPictures: Array<PlantPicture>) => void,
}

const PlantCreateOrEditComponent: React.FC<PlantCreateOrEditComponentProps> = (props) => {
    const [plantPictures, setPlantPictures] = useState<Array<PlantPicture>>([]);

    const formik = useFormik({
        initialValues: props.plant,

        validationSchema: Yup.object({
            name: Yup.string()
                .max(2024, 'Must be 2028 characters or less')
                .min(3, 'Must be 3 characters or more')
                .required('Required')
        }),

        onSubmit: (plant: Plant) => props.onSubmit(plant, plantPictures)
    });

    useEffect(() => {

    }, []);

    return (
        <form onSubmit={formik.handleSubmit}>

            <CardContent>
                <Grid container spacing={2}>
                    <Grid item xs={8}>
                        <TextField
                            id="name"
                            name="name"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                        />
                    </Grid>
                </Grid>

                <MultipleFileUpload
                    files={plantPictures}
                    onChange={(files: Array<any>) => setPlantPictures(files)}
                />
            </CardContent>

            <CardActions>
                <Button size="small" type="submit">Create</Button>
                {/*<Button size="small" onClick={() => navigation("/plants")}>Cancel</Button>*/}
            </CardActions>
        </form>
    );
}

export default PlantCreateOrEditComponent;
