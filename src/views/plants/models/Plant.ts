export default interface Plant {
    name: string | null;
    id: number | null;
    potId: number | null;
}

export const DEFAULT: Plant = {
    name: null,
    id: null,
    potId: 1
}