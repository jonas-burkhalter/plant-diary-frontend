export default interface PlantPicture {
    id: number | null;
    filename: string | null;
    data: any;
}

export const DEFAULT: PlantPicture = {
    id: null,
    filename: null,
    data: null,
}