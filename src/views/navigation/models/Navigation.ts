export default interface Navigation {
  id: number;
  label: string;
  order: number;
  url: string;
  icon: string;
}
