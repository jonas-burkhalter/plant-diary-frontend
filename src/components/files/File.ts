export default interface File {
    data: string | null;
    filename: string | null;
}

export const DEFAULT: File = {
    data: null,
    filename: null
}