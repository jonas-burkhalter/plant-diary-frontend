import React, {useState} from "react";

import {toBase64} from "./FileConverter";
import File from "./File";
import {Fab, Grid} from "@mui/material";

interface FileUploadMultipleProps {
    files?: Array<File>

    onChange: (files: Array<File>) => void
}

const MultipleFileUpload: React.FC<FileUploadMultipleProps> = (props) => {
    async function upload(event: any) {
        if (event.currentTarget !== null && event.currentTarget.files !== null) {
            const filename: string = event.currentTarget.files[0].name;
            const data: string = await toBase64(event.currentTarget.files[0]);

            props.files?.push({
                data: data,
                filename: filename
            })
        }
    }

    return (
        <Grid container spacing={2}>
            {props.files?.map((file: File) =>
                <Grid item xs={6}>
                    {file.filename}adsf
                </Grid>
            )}

            {/*<input*/}
            {/*    accept="image/*"*/}
            {/*    id="contained-button-file"*/}
            {/*    multiple*/}
            {/*    type="file"*/}
            {/*    onChange={upload}*/}
            {/*/>*/}

            {/*<Fab component="span">*/}
            {/*    /!*<AddPhotoAlternateIcon />*!/*/}
            {/*</Fab>*/}
        </Grid>
    );
}

export default MultipleFileUpload;


