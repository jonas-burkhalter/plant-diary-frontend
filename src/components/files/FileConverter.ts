export const toBase64 = (file: any): Promise<string> => {
    return new Promise(resolve => {
        const fileReader: FileReader = new FileReader();
        fileReader.readAsDataURL(file);
        fileReader.onload = (e: any) => {
            const base64String = e.target['result'] as string;
            const base64Data = base64String.substring(base64String.indexOf('base64,') + 'base64,'.length);
            resolve(base64Data);
        };
    });
};
